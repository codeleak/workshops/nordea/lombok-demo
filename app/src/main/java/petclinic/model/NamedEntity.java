package petclinic.model;

import lombok.*;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
abstract class NamedEntity extends BaseEntity {

    private String name;

}
