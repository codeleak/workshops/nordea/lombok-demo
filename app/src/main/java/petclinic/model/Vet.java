package petclinic.model;

import lombok.*;

import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = true)
public class Vet extends Person {

    private final Set<Specialty> specialties;

    @Builder
    public Vet(String firstName, String lastName, @Singular Set<Specialty> specialties) {
        super(firstName, lastName);
        this.specialties = specialties;
    }
}
