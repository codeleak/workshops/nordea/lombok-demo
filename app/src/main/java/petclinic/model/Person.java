package petclinic.model;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
abstract class Person extends BaseEntity {

    private String firstName;
    private String lastName;

}
