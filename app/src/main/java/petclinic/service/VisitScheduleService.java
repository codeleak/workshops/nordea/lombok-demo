package petclinic.service;

import petclinic.model.Pet;
import petclinic.model.Visit;

public interface VisitScheduleService {
    Visit scheduleVisit(Pet pet);
}
